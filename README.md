
# Face recognition by using Lambda 

## Overview

**AWS Lambda** is a compute service that lets you run code **without provisioning or managing servers**. AWS Lambda executes your code only when needed and scales automatically. You can run code for virtually any type of application or backend service - **all with zero administration**. AWS Lambda runs your code on a **high-availability compute infrastructure** and performs all of the administration of the compute resources, including server and operating system maintenance, capacity provisioning and automatic scaling, code monitoring and logging. 

**AWS Lambda layer** let you configure your Lambda function to **pull in additional code** and **content in the form of layers**. A layer is a **ZIP archive** that contains libraries, a custom runtime, or other dependencies. With layers, you can use libraries in your function without needing to include them in your deployment package.


## Scenario
The following procedures help you easily build the Face recognition program, you just upload your image to S3 bucket, and the all procedure will automatically work, you don't need to install any program or host a server , AWS Lambda will do everything, you can easily identify a image in the end. 

<p align="center">
     <img src="IMAGES/Lambda.png" width="350" height="500" >
</p>

## Prerequisites
- Download [haarcascade_frontalface_default.xml](haarcascade_frontalface_default.xml)
- Download [man.jpg](man.jpg)


## Lab tutorial

### Create IAM  Role 
- On the **Service** menu, select **IAM**.

- In the navigation pane, click **Roles**

- Click **Create role**

- Choose **EC2**, then click **Next: Permissions**


<center>
 <img src="IMAGES/1.png" width="85%" height="85%">
</center> 


- In **Filter policies**, type **s3**, and select **AmazonS3FullAccess**

<p align="center">
    <img src="IMAGES/2.png" width="85%" height="85%">
</p>

- Click **Next: Tags**

- Click **Next: Review**

- For Role name, type `EC2ToS3_your name`

<p align="center">
    <img src="IMAGES/3.png" width="85%" height="85%">
</p>

- Click **Create role**

- Choose **Lambda**, then click **Next: Permissions**

<p align="center">
    <img src="IMAGES/4.png" width="85%" height="85%">
</p>

- In **Filter policies**, type **s3**, and select **AmazonS3FullAccess**

<p align="center">
    <img src="IMAGES/5.png" width="85%" height="85%">
</p>

- Click **Netxt: Tags**

- Click **Next: Review**

- For Role name, type `LambdaToS3_your name`

<p align="center">
    <img src="IMAGES/6.png" width="85%" height="85%">
</p>

- Click **Create role**

### Create S3 bucket 
- On the **Service** menu, select **S3**.

- Create **Create bucket**

- For **Bucket name**, type  `s3tolambda-your name`

> if you can't create the S3 bucket, please change to another name. 
> Because you have to give unique name in S3 bucket 

- Click **Create**

<p align="center">
    <img src="IMAGES/7.png" width="85%" height="85%">
</p>

- Find your bucket, and click it 

- Click **Create folder**, and type `input-data`

<p align="center">
    <img src="IMAGES/8.png" width="85%" height="85%">
</p>

- Click **Save**

- Click **Create folder** again, and type `output-data`

<p align="center">
    <img src="IMAGES/9.png" width="85%" height="85%">
</p>

- Click **Save**



### Launch EC2 instance 

- On the **Service** menu, select **EC2**.

- Click **Create Instance**

<p align="center">
    <img src="IMAGES/10.png" width="85%" height="85%">
</p>

- Select **Ubuntu Server 18.04 LTS (HVM), SSD Volume Type**, and click **Select**

<p align="center">
    <img src="IMAGES/11.png" width="85%" height="85%">
</p>

- Click **Next: Configure Instance Details**
- Enter the following details and click **Next: Add Storage** : 
    - Network : `Default VPC`
    - Auto-assign Public IP : `Enable`
    - IAM role : `EC2ToS3_your name`
> You also can select your own VPC to lauch EC2 instance 

<p align="center">
    <img src="IMAGES/12.png" width="85%" height="85%">
</p>

- Click **Next: Add Tags**

- Click **Add Tag**

- Enter the following details and click **Next Configure Security Group** :
    - Key : `Name`
    - Name : `Your name`

- Select **Create a new security group**, and enter the following details : 
    - Security group name: `EC2_Lambda_Your name`
    - Description: `Upload your package to S3`
> if your have your Security Group, you can use your own. But make sure you can login to EC2 instance 

<p align="center">
    <img src="IMAGES/13.png" width="85%" height="85%">
</p>

- Click **Review and Launch**

- Click **Launch**

- Choose **Choose an existing key pair** or **Create a new key pair** to make sure you can login EC2.

- Click **Launch Instances**

- Click **View Instances**

### Login your EC2 instnace 
- [For Windows users](https://gitlab.com/ecloudture/knowledge-base/aws-sop/aws-elastic-compute-cloud/tree/master/Launch%20and%20Login%20EC2#mac)

- [For Linux/MAC OS users](https://gitlab.com/ecloudture/knowledge-base/aws-sop/aws-elastic-compute-cloud/tree/master/Launch%20and%20Login%20EC2#mac)

- Use the following commands:
```
    sudo su
    apt-get update
    python3 -V       # check your python version is Python 3.6.x
    apt install python3-pip -y
    apt install awscli -y
    apt install zip -y
    mkdir -p build/python/lib/python3.6/site-packages
    pip3 install opencv-python -t build/python/lib/python3.6/site-packages
    cd build/
    zip -r package.zip .
    aws s3 cp package.zip s3://s3tolambda-hank    # xxx is your bucket name 
```
- On the **Service** menu, select **S3**.

- You can see the file you just uploaded, and click **package.zip**

<p align="center">
    <img src="IMAGES/14.png" width="85%" height="85%">
</p>

- Copy package.zip **Object URL** to your note. 


### Create Lambda Layer

- On the **Service** menu, select **Lambda**.

- In the navigation pane, click **Layers**

- Click **Create layer**

<p align="center">
    <img src="IMAGES/15.png" width="85%" height="85%">
</p>

- Enter the following details and click **Create** :
    - Name : `Your name`
    - Choose : `Upload a file from Amazon S3`
    - Amazon S3 link URL : `your Object URL`
    - Compatible runtimes - optional : `Python 3.6`

<p align="center">
    <img src="IMAGES/16.png" width="85%" height="85%">
</p>

- In the navigation pane, click **Functions**

- Click **Create function**

- Choose **Author from scratch**

<p align="center">
    <img src="IMAGES/17.png" width="85%" height="85%">
</p>

- Enter the following details and click **Create function** :
    - Function name : `Your name`
    - Runtime : `Python 3.6`
    - Execution role : `LambdaToS3_your name`

<p align="center">
    <img src="IMAGES/18.png" width="85%" height="85%">
</p>

- Click **Add trigger**

<p align="center">
    <img src="IMAGES/19.png" width="85%" height="85%">
</p>

- Enter the following details and click **Add** :
    - Choose S3
    - Bucket : `Your bucket name`
    - Event type : `PUT`
    - Prefix : `input-data/`

<p align="center">
    <img src="IMAGES/20.png" width="85%" height="85%">
</p>

- Click **Layers**

<p align="center">
    <img src="IMAGES/21.png" width="85%" height="85%">
</p>

- Click **Add a layer**

<p align="center">
    <img src="IMAGES/22.png" width="85%" height="85%">
</p>

- Enter the following details and click **Add** :
    - Choose **Select from list of runtime compatible layers**
    - Name : `Your name`
    - Version : `1`

<p align="center">
    <img src="IMAGES/23.png" width="85%" height="85%">
</p>

- Click to return your Lambda function 

<p align="center">
    <img src="IMAGES/24.png" width="85%" height="85%">
</p>

- In **Function code**, paste the following code in **lambda_handler.py**
```python
import os
import json
import boto3
import os
import json
import boto3
import cv2
faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
def lambda_handler(event, context):
    bucket = event['Records'][0]['s3']['bucket']['name']
    key_origin = event['Records'][0]['s3']['object']['key']
    s3 = boto3.client('s3')
    key=key_origin.replace("input-data/", "")
    s3.download_file(bucket, key_origin, '/tmp/' + key)
    before = cv2.imread('/tmp/' + key)
    gray = cv2.cvtColor(before, cv2.COLOR_RGB2GRAY)
    faces = faceCascade.detectMultiScale(gray,scaleFactor=1.1,minNeighbors=5,minSize=(30, 30),flags = cv2.CASCADE_SCALE_IMAGE)
    for (x, y, w, h) in faces:
        cv2.rectangle(before, (x, y), (x+w, y+h), (0, 255, 0), 2)
    cv2.imwrite('/tmp/after-' + key, before)
    s3.upload_file('/tmp/after-' + key, bucket, Key='output-data/after-' + key)
    return {
        "statusCode": 200,
        "body": "successful"
    }
```
<p align="center">
    <img src="IMAGES/25.png" width="85%" height="85%">
</p>

- Click **File**, select **New File** 

- Copy **haarcascade_frontalface_default.xml** code to this file 

- Click **File**, select **Save**

- For **Filename**, type `haarcascade_frontalface_default.xml`, and click **Save**

<p align="center">
    <img src="IMAGES/26.png" width="85%" height="85%">
</p>

- In **Basic settings**, enter the following detail :
    - Timeout : `1 min  0s`

<p align="center">
    <img src="IMAGES/27.png" width="85%" height="85%">
</p>

- Click **Save**

<p align="center">
    <img src="IMAGES/28.png" width="85%" height="85%">
</p>

### Test 

- On the **Service** menu, select **S3**.

- Go to your bucket, and upload **man.jpg** to your **input-data folder**

<p align="center">
    <img src="IMAGES/29.png" width="85%" height="85%">
</p>

- Go to **output-data folder**, check the result 

<p align="center">
    <img src="IMAGES/30.png" width="85%" height="85%">
</p>

- You will see the **after-man.jpg**, you can download this image to see the different between the two images



## Conclusion

Congratulations! You now have learned how to use Lambda , you can easy to build the severless application. If you want to use your own package to deploy, but Lambda don't support it, you can use Layer to import your own package to Lambda. With layers, you can use libraries in your function without needing to include them in your deployment package. Layers let you keep your deployment package small, which makes development easier. 



## Clean up 

- Lambda Function
- Lambda Layer
- EC2 instance
- S3 Bucket 
- IAM role 


## Appendix
- https://www.ecomottblog.com/?p=1488
- https://itnext.io/create-a-highly-scalable-image-processing-service-on-aws-lambda-and-api-gateway-in-10-minutes-7cbb2893a479
- https://docs.aws.amazon.com/lambda/latest/dg/configuration-layers.html

